using UnityEngine;
using UnityEngine.SceneManagement;

public class EntrySceneController : MonoBehaviour
{
    private const string CONST_UIMainSceneName = "UIMain";
    void Start()
    {
        SceneManager.LoadScene(CONST_UIMainSceneName);
    }
}
