using UnityEngine;

namespace KJGames.UI
{ 
    public class UIRoot : MonoBehaviour
    {
        #region Serialized Fields - Panels
        [SerializeField] private UIBaseView [] _Views;
        #endregion

        public T GetView<T>() where T : UIBaseView 
        {
            // TODO add dictionary<System.Type, UIBaseView>
            for (int i = 0; i < _Views.Length; i++)
            {
                if (_Views[i] is T)
                {
                    return (T)_Views[i];
                }
            }
            return default(T);
        }
    }
}
