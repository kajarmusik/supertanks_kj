using KJGames.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
public class UIMainPanelState : BaseState
{
    public UIMainPanelState(BaseState previousState) : base(previousState)
    {
        PreviousState = previousState;
    }

    private UIMainPanelView _View;
    private const string CONST_GameplaySceneName = "Gameplay";
    public override void PrepareState()
    {
        base.PrepareState();

        _View = Owner.UI.GetView<UIMainPanelView>();
        CreateDataModel();

        _View.Init();
        _View.Show();
    }
    public override void UpdateState()
    {
        base.UpdateState();
    }
    public override void DestroyState()
    {
        base.DestroyState();

        _View.DeInit();
        _View.Hide();
    }
    private void CreateDataModel() 
    {
        UIMainPanelDataModel testPanelDataModel = new UIMainPanelDataModel()
        {
            OnPlayClicked = Play,
            OnExitClicked = Exit
        };

        _View.SetData(testPanelDataModel);
    }
    private void Play() 
    {
       SceneManager.LoadScene(CONST_GameplaySceneName);
    }
    private void Exit() 
    {
        Application.Quit();
    }
}
