public abstract class BaseModel
{
    public BaseModel() 
    {
    
    }
    public virtual void PrepareModel() { }
    public virtual void DestroyModel() { }
}
