using KJGames.UI;
using System;

public class UIMainPanelDataModel : DataModel 
{
    public Action OnPlayClicked;
    public Action OnExitClicked;
}
public class UIMainPanelView : UIPanelView
{
    UIMainPanelDataModel _DataModel => (UIMainPanelDataModel)DataModel;

    public void OnPlayBtnClicked() 
    {
        _DataModel.OnPlayClicked?.Invoke();
    }
    public void OnExitBtnClicked() 
    {
        _DataModel.OnExitClicked?.Invoke();
    }
}
