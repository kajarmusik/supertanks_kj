namespace KJGames.UI
{
    public abstract class BaseState
    {
        public UIStateMachine Owner;
        public BaseState PreviousState;

        public BaseState()
        {

        }

        public BaseState(BaseState previousState)
        {
            PreviousState = previousState;
        }

        public virtual void PrepareState() { }

        public virtual void UpdateState() { }

        public virtual void DestroyState() { }
    }
}

