using UnityEngine;

namespace KJGames.UI
{ 
    public class UIBaseView : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField] private Canvas _Canvas;
        #endregion

        #region Virtual Methods
        public virtual void Show()
        {
            _Canvas.enabled = true;
        }
        public virtual void Hide()
        {
            if (_Canvas != null && _Canvas.gameObject != null)
            {
                _Canvas.enabled = false;
            }
        }
        public virtual void Init() { }
        public virtual void DeInit() { }
        public virtual void UpdateView() { }
        #endregion
    }
}