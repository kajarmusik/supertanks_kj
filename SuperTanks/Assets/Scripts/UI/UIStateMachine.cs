using UnityEngine;

namespace KJGames.UI
{
    public class UIStateMachine : MonoBehaviour
    {
        [SerializeField] private UIRoot _UI;

        public BaseState CurrentState => _CurrentState;

        public UIRoot UI => _UI;
        private BaseState _CurrentState;

        private void OnDisable()
        {
            _CurrentState?.DestroyState();
        }
        private void Update()
        {
            if (_CurrentState != null)
            {
                _CurrentState.UpdateState();
            }
        }
        public void ChangeState(BaseState newState)
        {
            if (_CurrentState != null)
            {
                _CurrentState.DestroyState();
            }

            _CurrentState = newState;

            if (_CurrentState != null)
            {
                _CurrentState.Owner = this;
                _CurrentState.PrepareState();
            }
        }
    }
}

