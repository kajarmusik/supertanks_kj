public class IdleTankState : BaseTankState
{
    private Player _Player;
    public IdleTankState(Player player) 
    {
        _Player = player;
    }

    public override void Enter()
    {
        base.Enter();
        _Player.ActivePlayerPointer.SetActive(false);
    }

    public override void Exit()
    {
        base.Exit();
        _Player.ActivePlayerPointer.SetActive(true);
    }
}
