using UnityEngine;
public class AimTankState : BaseTankState
{
    private Player _Player;

    public AimTankState(Player player) 
    {
        _Player = player;
    }

    public override void Enter()
    {
        base.Enter();

        _Player.Pointer.SetActive(true);
    }
    public override void Update(float value)
    {
        base.Update(value);

        _Player.Pointer.transform.position = _Player.GameplayCamera.GetMouseWorldPosition();

        Vector3 diff = _Player.Pointer.transform.position - _Player.Turret.transform.position;
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        _Player.Turret.transform.rotation = Quaternion.Euler(0.0f, 0f, rot_z - 90.0f);

        if (Input.GetKeyUp(KeyCode.Space))
        {
            _Player.CurrentState = new ShootTankState(_Player);
        }
    }
}
