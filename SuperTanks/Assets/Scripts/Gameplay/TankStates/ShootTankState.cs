using UnityEngine;
public class ShootTankState : BaseTankState
{
    private Player _Player;
    private Vector2 _Force = Vector2.zero;
    public ShootTankState(Player player)
    {
        _Player = player;
    }
    public override void Enter()
    {
        base.Enter();

        PrepareBullet();
        Shoot();

        _Player.CurrentState = new IdleTankState(_Player);
    }
    public override void Exit()
    {
        base.Exit();

        _Player.Pointer.SetActive(false);      
        _Player.OnTurnEnd?.Invoke();
    }
    private void PrepareBullet() 
    {
        _Player.Bullet.PlayerOwner = _Player;
        _Player.Bullet.Rigidbody2D.angularVelocity = 0f;
        _Player.Bullet.Rigidbody2D.velocity = Vector2.zero;      
        _Player.Bullet.transform.position = _Player.BulletSpawnPoint.position;
        _Player.Bullet.transform.rotation = _Player.BulletSpawnPoint.rotation;
        _Player.Bullet.gameObject.SetActive(true);
    }
    private void Shoot()
    {
        _Force = (_Player.GameplayCamera.GetMouseWorldPosition() - _Player.Bullet.transform.position).normalized * 300;
        _Player.Bullet.Rigidbody2D.AddForce(_Force);
    }
}
