using UnityEngine;

public class MoveTankState : BaseTankState
{
    private float _Axis;
    private Player _Player;

    private const string CONST_AxisName = "Horizontal";

    public MoveTankState(Player player) 
    {
        _Player = player;
    }
    public override void Update(float value)
    {
        base.Update(value);

        _Axis = Input.GetAxis(CONST_AxisName);

        _Axis = _Axis < 0.0f ? 0.0f : (_Axis > 0.0f ? 1.0f : 0.5f);

        Vector2 direction = Vector3.Lerp(-_Player.transform.right, _Player.transform.right, _Axis);
        _Player.TankRigidbody.AddForce(direction * _Player.TankSpeed);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _Player.CurrentState = new AimTankState(_Player);
        }
    }
}
