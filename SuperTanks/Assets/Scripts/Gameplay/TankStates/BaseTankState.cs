public abstract class BaseTankState
{
   public virtual void Enter() { }
   public virtual void Update(float deltaTime) { }
   public virtual void Exit() { }
}
