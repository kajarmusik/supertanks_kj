using UnityEngine;

public class GameplayCamera : MonoBehaviour
{
    [SerializeField] private Camera _Camera;

    private Plane _Plane;

    public Vector3 GetMouseWorldPosition() 
    {
        Ray ray = _Camera.ScreenPointToRay(Input.mousePosition);
        if (_Plane.Raycast(ray, out float distance))
        {
            return ray.GetPoint(distance);
        }
        return Vector3.zero;
    }
    void Start()
    {
        _Plane = new Plane(Vector3.forward, 0.0f);
    }
}
