using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    public Action OnTurnEnd;
    public Action OnPointsChanged;

    #region Serialized Fields
    [SerializeField] private GameplayCamera _GameplayCamera;
    [SerializeField] private Rigidbody2D _TankRigidbody;
    [SerializeField] private float _Speed;
    [SerializeField] private GameObject _Pointer;
    [SerializeField] private Transform _Turret;
    [SerializeField] private Transform _BulletSpawnPoint;
    [SerializeField] private Bullet _Bullet;
    [SerializeField] private TMPro.TextMeshPro _LabelPoints;
    [SerializeField] private GameObject _ActivePlayerPointer;
    #endregion

    #region Private Fields
    private int _Points = 0;
    private BaseTankState _CurrentState;
    #endregion

    #region Properties
    public GameObject ActivePlayerPointer => _ActivePlayerPointer;
    public Rigidbody2D TankRigidbody => _TankRigidbody;
    public GameObject Pointer => _Pointer;
    public Transform Turret => _Turret;
    public GameplayCamera GameplayCamera => _GameplayCamera;
    public Bullet Bullet => _Bullet;
    public Transform BulletSpawnPoint => _BulletSpawnPoint;
    public float TankSpeed => _Speed;
    public int Points 
    { 
        get => _Points;
        set 
        {
            _Points = value;
            _LabelPoints.text = value.ToString();
            OnPointsChanged?.Invoke();
        }
    }
    public BaseTankState CurrentState {
        get => _CurrentState;
        set
        {
            _CurrentState?.Exit();
            _CurrentState = value;
            _CurrentState?.Enter();
        }
    }
    #endregion

    void Update()
    {
        _CurrentState?.Update(Time.deltaTime);

        transform.up = Vector3.Lerp(transform.up, Vector3.up, Time.deltaTime * 10);

        _ActivePlayerPointer.transform.localScale = Vector3.Lerp(Vector3.one * 0.5f, Vector3.one * 0.7f, Mathf.Abs(Mathf.Sin(Time.time * 2.0f)));
        _LabelPoints.transform.rotation = Quaternion.identity;
    }
}
