using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Player PlayerOwner;

    [SerializeField] private Rigidbody2D _Rigidbody2D;

    public Rigidbody2D Rigidbody2D => _Rigidbody2D;

    private const string CONST_PlayerTag = "Player";

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(CONST_PlayerTag))
        {
            PlayerOwner.Points++;
            gameObject.SetActive(false);
        }
    }
}
