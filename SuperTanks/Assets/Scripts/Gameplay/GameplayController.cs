using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayController : MonoBehaviour
{
    [SerializeField] private Player _Player;
    [SerializeField] private int _PlayersCount;
    [SerializeField] private Transform[] _PlayersSpawnPoints;

    private List<Player> _PlayersList = new List<Player>();
    private Player _ActivePlayer;
    private int _ActivePlayerIndex = 0;

    private const string CONST_MenuSceneName = "UIMain";
    public Player ActivePlayer
    {
        get => _ActivePlayer;
        set
        {
            if (_ActivePlayer != null)
            {
                _ActivePlayer.OnTurnEnd = null;
            }
            _ActivePlayer = value;
            if (_ActivePlayer != null)
            {
                _ActivePlayer.OnTurnEnd = SetNextPlayerActive;
            }
        }
    }

    private void Start()
    {
        for (int i = 0; i < _PlayersCount; i++)
        {
            int randomSpawnIndex = Random.Range(0, _PlayersSpawnPoints.Length);

            Player newPlayer = Instantiate(_Player);
            _PlayersList.Add(newPlayer);
            newPlayer.transform.position = _PlayersSpawnPoints[randomSpawnIndex].position;
            newPlayer.gameObject.SetActive(true);
            newPlayer.CurrentState = new IdleTankState(newPlayer);
            newPlayer.Points = 0;
            newPlayer.OnPointsChanged = CheckPoints;
            newPlayer.gameObject.name = $"PlayerName_{i}";
        }
        ActivePlayer = _PlayersList[0];
        ActivePlayer.CurrentState = new MoveTankState(ActivePlayer);
    }
    private void CheckPoints()
    {
        for (int i = 0; i < _PlayersCount; i++)
        {
            if (_PlayersList[i].Points == 10)
            {
                GameOver(_PlayersList[i]);
            }
        }
    }
    private void GameOver(Player player) 
    {
        Debug.Log(player.gameObject.name + " WIN!");
        SceneManager.LoadScene(CONST_MenuSceneName);

    }
    private void SetNextPlayerActive()
    {
        _ActivePlayerIndex++;

        if (_ActivePlayerIndex >= _PlayersList.Count) 
        {
            _ActivePlayerIndex = 0;
        }
        ActivePlayer = _PlayersList[_ActivePlayerIndex];
        ActivePlayer.CurrentState = new MoveTankState(ActivePlayer);
    }
}
