using UnityEngine;
using KJGames.UI;

public class MainSceneController : MonoBehaviour
{
    [SerializeField] private UIStateMachine _UIStateMachine;

    private void Start()
    {
        UIMainPanelState mainPanelState = new UIMainPanelState(_UIStateMachine.CurrentState);
        _UIStateMachine.ChangeState(mainPanelState);
    }
}
